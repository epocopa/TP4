package es.ucm.fdi.constructor;

import es.ucm.fdi.ini.IniSection;
import es.ucm.fdi.model.events.Evento;
import es.ucm.fdi.model.events.EventoNuevoVehiculo;

public class ConstructorEventoNuevoVehiculo extends ConstructorEventos {

	public ConstructorEventoNuevoVehiculo() {
		etiqueta = "new_vehicle";
		claves = new String[]{"time", "id"};
		valoresPorDefecto = new String[]{"", ""};
	}

	@Override
	public Evento parser(IniSection section) {
		if (!section.getTag().equals(etiqueta)
				|| section.getValue("type") != null) {
			return null;
		} else {
			return new EventoNuevoVehiculo(
					// extrae el valor del campo “time” en la sección
					// 0 es el valor por defecto en caso de no especificar el tiempo
					parseaIntNoNegativo(section, "time", 0),
					// extrae el valor del campo “id” de la sección
					identificadorValido(section, "id"),
					parseaIntNoNegativo(section, "max_speed", 0),
					section.getValue("itinerary").split(",")
			);
		}
	}

	@Override
	public String toString() {
		return "New Vehicle";
	}
}