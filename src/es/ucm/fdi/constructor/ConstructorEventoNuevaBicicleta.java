package es.ucm.fdi.constructor;

import es.ucm.fdi.ini.IniSection;
import es.ucm.fdi.model.events.Evento;
import es.ucm.fdi.model.events.EventoNuevaBicicleta;

public class ConstructorEventoNuevaBicicleta extends ConstructorEventos {
	public ConstructorEventoNuevaBicicleta() {
		etiqueta = "new_vehicle";
		claves = new String[]{"time", "id"};
		valoresPorDefecto = new String[]{"", ""};
	}

	@Override
	public Evento parser(IniSection section) {
		if (!section.getTag().equals(etiqueta) || !section.getValue("type").equals("bike")) {
			return null;
		} else {
			return new EventoNuevaBicicleta(
					parseaIntNoNegativo(section, "time", 0),
					identificadorValido(section, "id"),
					parseaIntNoNegativo(section, "max_speed", 0),
					section.getValue("itinerary").split(",")
			);
		}
	}
}
