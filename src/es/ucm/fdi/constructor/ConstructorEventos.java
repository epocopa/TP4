package es.ucm.fdi.constructor;

import es.ucm.fdi.ini.IniSection;
import es.ucm.fdi.model.events.Evento;

public abstract class ConstructorEventos {
	protected String etiqueta; // etiqueta de la entrada (“new_road”, etc..)
	protected String[] claves; // campos de la entrada (“time”, “vehicles”, etc.)
	protected String[] valoresPorDefecto;

	public ConstructorEventos() {
		etiqueta = null;
		claves = null;
	}
	public abstract Evento parser(IniSection section);

	protected static String identificadorValido(IniSection seccion, String clave) {
		String s = seccion.getValue(clave);
		if (!esIdentificadorValido(s)) {
			throw new IllegalArgumentException("El valor " + s + " para " + clave + " no es un ID valido");
		} else {
			return s;
		}
	}

	private static boolean esIdentificadorValido(String id) {
		return id != null && id.matches("[a-z0-9_]+");
	}

	protected static int parseaInt(IniSection seccion, String clave) {
		String v = seccion.getValue(clave);
		if (v == null){
			throw new IllegalArgumentException("Valor inexistente para la clave: " + clave);
		} else {
			return Integer.parseInt(seccion.getValue(clave));
		}
	}

	protected static int parseaInt(IniSection seccion, String clave, int valorPorDefecto) {
		String v = seccion.getValue(clave);
		return (v != null) ? Integer.parseInt(seccion.getValue(clave)) : valorPorDefecto;
	}

	protected static int parseaIntNoNegativo(IniSection seccion, String clave, int valorPorDefecto) {
		int i = parseaInt(seccion, clave, valorPorDefecto);
		if (i < 0){
			throw new IllegalArgumentException("El valor " + i + " para " + clave + " es negativo");
		} else {
			return i;
		}
	}

	protected static Double parseaDouble(IniSection seccion, String clave, double valorPorDefecto) {
		String v = seccion.getValue(clave);
		return (v != null) ? Double.parseDouble(seccion.getValue(clave)) : valorPorDefecto;
	}

	protected static Double parseaDoubleNoNegativo(IniSection seccion, String clave, double valorPorDefecto) {
		double d = parseaDouble(seccion, clave, valorPorDefecto);
		if (d < 0){
			throw new IllegalArgumentException("El valor " + d + " para " + clave + "  es negativo ");
		} else {
			return d;
		}
	}

	protected static long parseaLong(IniSection seccion, String clave, long valorPorDefecto) {
		String v = seccion.getValue(clave);
		return (v != null) ? Long.parseLong(seccion.getValue(clave)) : valorPorDefecto;
	}
}
