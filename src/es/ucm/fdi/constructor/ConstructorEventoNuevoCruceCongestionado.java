package es.ucm.fdi.constructor;

import es.ucm.fdi.ini.IniSection;
import es.ucm.fdi.model.events.Evento;
import es.ucm.fdi.model.events.EventoNuevoCruceCongestionado;

public class ConstructorEventoNuevoCruceCongestionado extends ConstructorEventoNuevoCruce{
	
	public ConstructorEventoNuevoCruceCongestionado() {
		etiqueta = "new_junction";
		claves = new String[]{"time", "id"};
		valoresPorDefecto = new String[]{"", ""};
	}

	@Override
	public Evento parser(IniSection section) {
		if (!section.getTag().equals(etiqueta) || !section.getValue("type").equals("mc")) {
			return null;
		} else {
			return new EventoNuevoCruceCongestionado(
					parseaIntNoNegativo(section, "time", 0),
					identificadorValido(section, "id")
			);
		}
	}
}
