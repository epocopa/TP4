package es.ucm.fdi.constructor;

import es.ucm.fdi.ini.IniSection;
import es.ucm.fdi.model.events.Evento;
import es.ucm.fdi.model.events.EventoNuevoCamino;

public class ConstructorEventoNuevoCamino extends ConstructorEventoNuevaCarretera {

	public ConstructorEventoNuevoCamino() {
		etiqueta = "new_road";
		claves = new String[]{"time", "id"};
		valoresPorDefecto = new String[]{"", ""};
	}

	@Override
	public Evento parser(IniSection section) {
		if (!section.getTag().equals(etiqueta) || !section.getValue("type").equals("dirt")) {
			return null;
		} else {
			return new EventoNuevoCamino(
					parseaIntNoNegativo(section, "time", 0),
					identificadorValido(section, "id"),
					parseaIntNoNegativo(section, "max_speed", 0),
					parseaIntNoNegativo(section, "length", 0),
					identificadorValido(section, "src"),
					identificadorValido(section, "dest")
			);
		}
	}
}
