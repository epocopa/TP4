package es.ucm.fdi.constructor;

import es.ucm.fdi.ini.IniSection;
import es.ucm.fdi.model.events.Evento;
import es.ucm.fdi.model.events.EventoNuevoCruceCircular;

public class ConstructorEventoNuevoCruceCircular extends ConstructorEventoNuevoCruce {
	
	public ConstructorEventoNuevoCruceCircular() {
		etiqueta = "new_junction";
		claves = new String[]{"time", "id"};
		valoresPorDefecto = new String[]{"", ""};
	}

	@Override
	public Evento parser(IniSection section) {
		if (!section.getTag().equals(etiqueta) || !section.getValue("type").equals("rr")) {
			return null;
		} else {
			return new EventoNuevoCruceCircular(
					parseaIntNoNegativo(section, "time", 0),
					identificadorValido(section, "id"),
					parseaIntNoNegativo(section, "max_time_slice", 0),
					parseaIntNoNegativo(section, "min_time_slice", 0)
			);
		}
	}
}
