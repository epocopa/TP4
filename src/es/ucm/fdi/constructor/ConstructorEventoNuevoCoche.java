package es.ucm.fdi.constructor;

import es.ucm.fdi.ini.IniSection;
import es.ucm.fdi.model.events.Evento;
import es.ucm.fdi.model.events.EventoNuevoCoche;

public class ConstructorEventoNuevoCoche extends ConstructorEventos {
	public ConstructorEventoNuevoCoche() {
		etiqueta = "new_vehicle";
		claves = new String[]{"time", "id"};
		valoresPorDefecto = new String[]{"", ""};
	}

	@Override
	public Evento parser(IniSection section) {
		if (!section.getTag().equals(etiqueta) || !section.getValue("type").equals("car")) {
			return null;
		} else {
			return new EventoNuevoCoche(
					parseaIntNoNegativo(section, "time", 0),
					identificadorValido(section, "id"),
					parseaIntNoNegativo(section, "max_speed", 0),
					section.getValue("itinerary").split(","),
					parseaIntNoNegativo(section, "resistance", 0),
					parseaDoubleNoNegativo(section, "fault_probability", 0),
					parseaIntNoNegativo(section, "max_fault_duration", 0),
					parseaLong(section, "seed", 0)
			);
		}
	}
}
