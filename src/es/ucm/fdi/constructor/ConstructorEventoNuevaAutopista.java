package es.ucm.fdi.constructor;

import es.ucm.fdi.ini.IniSection;
import es.ucm.fdi.model.events.Evento;
import es.ucm.fdi.model.events.EventoNuevaAutopista;

public class ConstructorEventoNuevaAutopista extends ConstructorEventos {

	public ConstructorEventoNuevaAutopista() {
		etiqueta = "new_road";
		claves = new String[]{"time", "id"};
		valoresPorDefecto = new String[]{"", ""};
	}

	@Override
	public Evento parser(IniSection section) {
		if (!section.getTag().equals(etiqueta) || !section.getValue("type").equals("lanes")) {
			return null;
		} else {
			return new EventoNuevaAutopista(
					parseaIntNoNegativo(section, "time", 0),
					identificadorValido(section, "id"),
					parseaIntNoNegativo(section, "max_speed", 0),
					parseaIntNoNegativo(section, "length", 0),
					identificadorValido(section, "src"),
					identificadorValido(section, "dest"),
					parseaIntNoNegativo(section, "lanes", 0)
			);
		}
	}
}
