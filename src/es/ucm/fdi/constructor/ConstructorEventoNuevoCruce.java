package es.ucm.fdi.constructor;

import es.ucm.fdi.ini.IniSection;
import es.ucm.fdi.model.events.Evento;
import es.ucm.fdi.model.events.EventoNuevoCruce;

public class ConstructorEventoNuevoCruce extends ConstructorEventos {

	public ConstructorEventoNuevoCruce() {
		etiqueta = "new_junction";
		claves = new String[]{"time", "id"};
		valoresPorDefecto = new String[]{"", ""};
	}

	@Override
	public Evento parser(IniSection section) {
		if (!section.getTag().equals(etiqueta)
				|| section.getValue("type") != null) {
			return null;
		} else {
			return new EventoNuevoCruce(
					parseaIntNoNegativo(section, "time", 0),
					identificadorValido(section, "id"));
		}
	}

	@Override
	public String toString() {
		return "New Junction";
	}
}
