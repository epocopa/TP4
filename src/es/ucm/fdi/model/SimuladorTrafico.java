package es.ucm.fdi.model;

import es.ucm.fdi.model.events.Evento;
import es.ucm.fdi.util.SortedArrayList;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Comparator;
import java.util.List;

public class SimuladorTrafico {
	private MapaCarreteras mapa;
	private List<Evento> eventos;
	private int contadorTiempo;

	public SimuladorTrafico() {
		mapa = new MapaCarreteras();
		contadorTiempo = 0;
		Comparator<Evento> cmp = new Comparator<Evento>() {
			@Override
			public int compare(Evento evento, Evento t1) {
				if (evento.getTiempo() == t1.getTiempo()){
					return 0;
				} else if (evento.getTiempo() < t1.getTiempo()){
					return -1;
				} else {
					return 1;
				}
			}
		};
		eventos = new SortedArrayList<>(cmp); // estructura ordenada por “tiempo”
	}

	public void ejecuta(int pasosSimulacion, OutputStream ficheroSalida) {
		int limiteTiempo = contadorTiempo + pasosSimulacion - 1;
		while ( contadorTiempo <= limiteTiempo ) {
			for (Evento e : eventos){
				if (e.getTiempo() == contadorTiempo){
					e.ejecuta(mapa);
				}
			}
			contadorTiempo++;
			mapa.actualizar();
			//PrintWriter y try-with-resources?
			PrintStream p = new PrintStream(ficheroSalida);
			p.print(mapa.generateReport(contadorTiempo));
		}
	}

	public void insertaEvento(Evento e) {
		//No hace falta excepcion porque Parsea int no negativo
		eventos.add(e);
	}
}
