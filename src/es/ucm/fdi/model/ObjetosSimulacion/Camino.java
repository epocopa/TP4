package es.ucm.fdi.model.ObjetosSimulacion;

import es.ucm.fdi.ini.IniSection;

public class Camino extends Carretera {

	public Camino(String id, int longitud, int velocidadMaxima, CruceGenerico<?> cruceOrigen, CruceGenerico<?> cruceDestino) {
		super(id, longitud, velocidadMaxima, cruceOrigen, cruceDestino);
	}

	@Override
	protected int calculaVelocidadBase() {
		return velocidadMaxima;
	}

	@Override
	protected int calculaFactorReduccion(int obstacles) {
		return ++obstacles;
	}

	@Override
	protected void completaDetallesSeccion(IniSection is) {
		super.completaDetallesSeccion(is);
		is.setValue("type", "dirt");
	}
}
