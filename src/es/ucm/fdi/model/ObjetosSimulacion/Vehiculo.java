package es.ucm.fdi.model.ObjetosSimulacion;

import java.util.List;

import es.ucm.fdi.control.ErrorDeSimulacion;
import es.ucm.fdi.ini.IniSection;

public class Vehiculo extends ObjetoSimulacion {
	protected Carretera carretera; // carretera en la que está el vehículo
	protected int velocidadMaxima; // velocidad máxima
	protected int velocidadActual; // velocidad actual
	protected int kilometraje; // distancia recorrida
	protected int localizacion; // localización en la carretera
	protected boolean haLlegado;
	protected boolean enCruce;
	protected int tiempoAveria; // tiempo que estará averiado
	protected List<CruceGenerico<?>> itinerario; // itinerario a recorrer (mínimo 2)
	protected int contadorCruce;

	public Vehiculo(String id, int velocidadMaxima, List<CruceGenerico<?>> itinerario) {
		super(id);
		if (velocidadMaxima < 0) {
			throw new ErrorDeSimulacion("Velocidad de vehiculo negativa");
		} else {
			this.velocidadMaxima = velocidadMaxima;
			this.itinerario = itinerario;
			carretera = null;
		}
	}

	public void avanza() {
		if (enCruce) {
			velocidadActual = 0;
			if (tiempoAveria > 0) {
				tiempoAveria--;
			}
			return;
		}

		if (tiempoAveria > 0) {
			tiempoAveria--;
		} else {
			localizacion += velocidadActual;
			kilometraje += velocidadActual;
			if (localizacion >= carretera.getLongitud()) {
				kilometraje -= (localizacion - carretera.getLongitud());
				localizacion = carretera.getLongitud();
				carretera.entraVehiculoAlCruce(this);
				enCruce = true;
				velocidadActual = 0;
			}
		}
	}

	public void moverASiguienteCarretera() {
		if (carretera != null) {
			carretera.saleVehiculo(this);
		}

		if (itinerario.size() - 1 == contadorCruce) {
			haLlegado = true;
			carretera = null;
			velocidadActual = 0;
			localizacion = 0;
		} else {
			CruceGenerico<?> origen = itinerario.get(contadorCruce);
			CruceGenerico<?> destino = itinerario.get(contadorCruce + 1);
			carretera = origen.carreteraHaciaCruce(destino);
			if (carretera == null) {
				throw new ErrorDeSimulacion("La carretera no existe");
			} else {
				localizacion = 0;
				carretera.entraVehiculo(this);
			}
			contadorCruce++;
		}
		enCruce = false;
	}

	public void setTiempoAveria(int tiempoAveria) {
		if (carretera == null) {
			return;
		}
		if (this.tiempoAveria > 0) {
			this.tiempoAveria += tiempoAveria;
		} else {
			this.tiempoAveria = tiempoAveria;
			velocidadActual = 0;
		}
	}

	public boolean estaAveriado() {
		return tiempoAveria > 0;
	}

	public void setVelActual(int nuevaVelocidad) {
		if (nuevaVelocidad < 0) {
			velocidadActual = 0;
		} else {
			velocidadActual = nuevaVelocidad > velocidadMaxima ? velocidadMaxima
					: nuevaVelocidad;
		}
	}

	@Override
	public String getNombreSeccion() {
		return "vehicle_report";
	}

	public int getLocalizacion() {
		return localizacion;
	}

	public int getTiempoAveria() {
		return tiempoAveria;
	}

	@Override
	public void completaDetallesSeccion(IniSection is) {
		is.setValue("speed", velocidadActual);
		is.setValue("kilometrage", kilometraje);
		is.setValue("faulty", tiempoAveria);
		is.setValue("location", haLlegado ? "arrived" : "(" + carretera.getId() + "," + getLocalizacion() + ")");
	}
}
