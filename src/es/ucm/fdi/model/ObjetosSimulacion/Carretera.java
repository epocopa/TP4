package es.ucm.fdi.model.ObjetosSimulacion;

import es.ucm.fdi.ini.IniSection;
import es.ucm.fdi.util.SortedArrayList;

import java.util.Comparator;
import java.util.List;

public class Carretera extends ObjetoSimulacion {
	protected int longitud; // longitud de la carretera
	protected int velocidadMaxima; // velocidad máxima
	protected CruceGenerico<?> cruceOrigen; // cruce del que parte la carretera
	protected CruceGenerico<?> cruceDestino; // cruce al que llega la carretera
	// lista ordenada de vehículos en la carretera (ordenada por localización)
	protected List<Vehiculo> vehiculos;
	protected Comparator<Vehiculo> comparadorVehiculo; // orden entre vehículos

	public Carretera(String id, int longitud, int velocidadMaxima, CruceGenerico<?> cruceOrigen, CruceGenerico<?> cruceDestino) {
		super(id);
		this.longitud = longitud;
		this.velocidadMaxima = velocidadMaxima;
		this.cruceOrigen = cruceOrigen;
		this.cruceDestino = cruceDestino;
		comparadorVehiculo = new Comparator<Vehiculo>() {
			@Override
			public int compare(Vehiculo vehiculo, Vehiculo t1) {
				if (vehiculo.getLocalizacion() == t1.getLocalizacion()) {
					return 0;
				} else if (vehiculo.getLocalizacion() < t1.getLocalizacion()) {
					return 1;
				} else {
					return -1;
				}
			}
		};
		vehiculos = new SortedArrayList<>(comparadorVehiculo);
	}

	@Override
	public void avanza() {
		int base = calculaVelocidadBase();
		int obstaculos = 0;
		for (Vehiculo vehiculo : vehiculos) {
			if (vehiculo.estaAveriado()){
				obstaculos++;
			} else {
				vehiculo.setVelActual(base / calculaFactorReduccion(obstaculos));
			}
			vehiculo.avanza();
		}
		vehiculos.sort(comparadorVehiculo);
	}

	public int getLongitud() {
		return longitud;
	}

	public void entraVehiculo(Vehiculo vehiculo) {
		// Si el vehículo no existe en la carretera, se añade a la lista de vehículos y
		// se ordena la lista. Si existe no se hace nada.
		if (!vehiculos.contains(vehiculo)) {
			vehiculos.add(vehiculo);
			vehiculos.sort(comparadorVehiculo);
		}
	}

	public void saleVehiculo(Vehiculo vehiculo) {
		// elimina el vehículo de la lista de vehículos
		vehiculos.remove(vehiculo);
	}

	public void entraVehiculoAlCruce(Vehiculo v) {
		// añade el vehículo al “cruceDestino” de la carretera”
		cruceDestino.entraVehiculoAlCruce(id, v);
	}

	protected int calculaVelocidadBase() {
		return Math.min(velocidadMaxima, (velocidadMaxima / (Math.max(vehiculos.size(), 1))+1));
	}

	protected int calculaFactorReduccion(int obstaculos) {
		// 1 si no hay obstaculos y 2 en caso contrario
		return obstaculos == 0 ? 1 : 2;
	}

	@Override
	protected String getNombreSeccion() {
		return "road_report";
	}

	@Override
	protected void completaDetallesSeccion(IniSection is) {
		String vehicles = "";
		for (Vehiculo vehiculo : vehiculos) {
			vehicles += "(" + vehiculo.getId() + "," + vehiculo.getLocalizacion() + ")" +
					(!vehiculo.equals(vehiculos.get(vehiculos.size() - 1)) ? "," : "");
		}
		is.setValue("state", vehicles);
	}
}
