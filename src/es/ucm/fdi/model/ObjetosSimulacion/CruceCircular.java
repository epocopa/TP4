package es.ucm.fdi.model.ObjetosSimulacion;

import es.ucm.fdi.ini.IniSection;

public class CruceCircular extends CruceGenerico<CarreteraEntranteConIntervalo> {
	private Integer minValorIntervalo;
	private Integer maxValorIntervalo;

	public CruceCircular(String id, Integer minValorIntervalo, Integer maxValorIntervalo) {
		super(id);
		this.maxValorIntervalo = maxValorIntervalo;
		this.minValorIntervalo = minValorIntervalo;
	}

	@Override
	protected CarreteraEntranteConIntervalo creaCarreteraEntrante(Carretera carretera) {
		return new CarreteraEntranteConIntervalo(carretera, maxValorIntervalo);
	}

	@Override
	protected void actualizaSemaforos() {
		if (indiceSemaforoVerde == -1){
			carreterasEntrantes.get(0).semaforo = true;
			indiceSemaforoVerde++;
		} else {
			CarreteraEntranteConIntervalo c = carreterasEntrantes.get(indiceSemaforoVerde);
			if (c.tiempoConsumido()){
				c.semaforo = false;
				if (c.getusoCompleto()){
					c.setIntervaloDeTiempo(Math.min(c.getIntervaloDeTiempo() + 1, maxValorIntervalo));
				}
				if (!c.usadaPorUnVehiculo()){
					c.setIntervaloDeTiempo(Math.max(c.getIntervaloDeTiempo() - 1, minValorIntervalo));
				}
				c.setUnidadesDeTiempoUsadas(0);
				c.setUsadaPorUnVehiculo(false);
				c.setUsoCompleto(true);
				indiceSemaforoVerde++;
				indiceSemaforoVerde %= carreterasEntrantes.size();
				carreterasEntrantes.get(indiceSemaforoVerde).semaforo = true;
			}
		}
	}

	@Override
	protected void completaDetallesSeccion(IniSection is) {
		String queues = "";

		for (CarreteraEntranteConIntervalo c : carreterasEntrantes) {
			queues += "(" + c.carretera.getId() + "," +
					(c.semaforo == true ? "green:" + (c.getIntervaloDeTiempo() - c.getUnidadesDeTiempoUsadas())
							: "red") + ",[";
			for (Vehiculo v : c.colaVehiculos) {
				queues += v.getId();
				if (!v.equals(c.colaVehiculos.get(c.colaVehiculos.size() - 1))) {
					queues += ",";
				}
			}

			queues += "])" + (!c.equals(carreterasEntrantes.get(carreterasEntrantes.size() - 1)) ? "," : "");

		}
		is.setValue("queues", queues);
		is.setValue("type", "rr");
	}
}
