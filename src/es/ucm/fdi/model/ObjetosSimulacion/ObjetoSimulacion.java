package es.ucm.fdi.model.ObjetosSimulacion;

import es.ucm.fdi.ini.IniSection;

public abstract class ObjetoSimulacion {
	protected String id;

	public ObjetoSimulacion(String id ) {
		this.id = id;
	}
	
	public String getId() {
		return id;

	}
	
	public String generarInforme(int tiempo) {
		IniSection is = new IniSection(getNombreSeccion());
		is.setValue("id", id);
		is.setValue("time", tiempo);
		completaDetallesSeccion (is);
		return is.toString();
	} 

	public abstract void avanza();
	protected abstract String getNombreSeccion();
	protected abstract void completaDetallesSeccion(IniSection is);


}
