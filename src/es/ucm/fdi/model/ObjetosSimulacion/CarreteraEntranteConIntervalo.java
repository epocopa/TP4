package es.ucm.fdi.model.ObjetosSimulacion;

public class CarreteraEntranteConIntervalo extends CarreteraEntrante {
	private int intervaloDeTiempo; 
	private int unidadesDeTiempoUsadas;
	private boolean usoCompleto;
	private boolean usadaPorUnVehiculo;

	protected CarreteraEntranteConIntervalo(Carretera carretera, int intervaloDeTiempo) {
		super(carretera);
		usoCompleto = true;
		this.intervaloDeTiempo = intervaloDeTiempo;
	}

	@Override
	protected void avanzaPrimerVehiculo() {
		unidadesDeTiempoUsadas++;
		if (colaVehiculos.isEmpty()) {
			usoCompleto = false;
		} else {
			colaVehiculos.get(0).moverASiguienteCarretera();
			colaVehiculos.remove(0);
			usadaPorUnVehiculo = true;
		}
	}

	public boolean tiempoConsumido() {
		return unidadesDeTiempoUsadas >= intervaloDeTiempo;
	}

	public boolean getusoCompleto() { 
		return usoCompleto;
	}

	public boolean usadaPorUnVehiculo() {
		return usadaPorUnVehiculo;
	}


	public void setIntervaloDeTiempo(int intervaloDeTiempo) {
		this.intervaloDeTiempo = intervaloDeTiempo;
	}
	public int getIntervaloDeTiempo() {
		return intervaloDeTiempo;
	}

	public void setUnidadesDeTiempoUsadas(int unidadesDeTiempoUsadas) {
		this.unidadesDeTiempoUsadas = unidadesDeTiempoUsadas;
	}

	public int getUnidadesDeTiempoUsadas() {
		return unidadesDeTiempoUsadas;
	}

	public void setUsoCompleto(boolean usoCompleto) {
		this.usoCompleto = usoCompleto;
	}

	public void setUsadaPorUnVehiculo(boolean usadaPorUnVehiculo) {
		this.usadaPorUnVehiculo = usadaPorUnVehiculo;
	}
}