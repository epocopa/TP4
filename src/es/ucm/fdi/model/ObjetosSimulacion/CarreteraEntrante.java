package es.ucm.fdi.model.ObjetosSimulacion;

import es.ucm.fdi.util.SortedArrayList;

import java.util.Comparator;
import java.util.List;

public class CarreteraEntrante {
	protected Carretera carretera;
	protected List<Vehiculo> colaVehiculos;
	protected boolean semaforo; // true=verde, false=rojo

	public CarreteraEntrante(Carretera carretera) {
		semaforo = false;
		this.carretera = carretera;
		colaVehiculos = new SortedArrayList<>(new Comparator<Vehiculo>() {
			@Override
			public int compare(Vehiculo vehiculo, Vehiculo t1) {
				if (vehiculo.getLocalizacion() == t1.getLocalizacion()) {
					return 0;
				} else if (vehiculo.getLocalizacion() < t1.getLocalizacion()) {
					return -1;
				} else {
					return 1;
				}
			}
		});
	}

	void ponSemaforo(boolean color) {
		semaforo = color;
	}

	protected void avanzaPrimerVehiculo() {
		if (colaVehiculos.size() != 0) {
			colaVehiculos.get(0).moverASiguienteCarretera();
			colaVehiculos.remove(0);
		}
	}
}