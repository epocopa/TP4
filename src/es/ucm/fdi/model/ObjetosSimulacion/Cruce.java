package es.ucm.fdi.model.ObjetosSimulacion;

public class Cruce extends CruceGenerico<CarreteraEntrante> {
	public Cruce(String id) {
		super(id);
	}

	@Override
	protected CarreteraEntrante creaCarreteraEntrante(Carretera carretera) {
		return new CarreteraEntrante(carretera);
	}

	protected void actualizaSemaforos() {
		// pone el semáforo de la carretera actual a “rojo”, y busca la siguiente
		// carretera entrante para ponerlo a “verde”
		if (indiceSemaforoVerde == -1) {
			carreterasEntrantes.get(0).semaforo = true;
			indiceSemaforoVerde++;
		} else {
			carreterasEntrantes.get(indiceSemaforoVerde).semaforo = false;
			indiceSemaforoVerde++;
			indiceSemaforoVerde %= carreterasEntrantes.size();
			carreterasEntrantes.get(indiceSemaforoVerde).semaforo = true;		
		}
	}
}
