package es.ucm.fdi.model.ObjetosSimulacion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.ucm.fdi.ini.IniSection;

abstract public class CruceGenerico<T extends CarreteraEntrante> extends ObjetoSimulacion {
	protected int indiceSemaforoVerde;
	protected List<T> carreterasEntrantes;
	protected Map<String, T> mapaCarreterasEntrantes;
	protected Map<CruceGenerico<?>, Carretera> mapaCarreterasSalientes;

	public CruceGenerico(String id) {
		super(id);
		carreterasEntrantes = new ArrayList<>();
		mapaCarreterasEntrantes = new HashMap<>();
		mapaCarreterasSalientes = new HashMap<>();
		indiceSemaforoVerde = -1;
	}

	public Carretera carreteraHaciaCruce(CruceGenerico<?> cruce) {
		// devuelve la carretera que llega a ese cruce desde “this”
		return mapaCarreterasSalientes.get(cruce);
	}

	public void addCarreteraEntranteAlCruce(String idCarretera, Carretera carretera) {
		// añade una carretera entrante al “mapaCarreterasEntrantes” y // a las “carreterasEntrantes”
		T cEntrante = creaCarreteraEntrante(carretera);
		mapaCarreterasEntrantes.put(idCarretera, cEntrante);
		carreterasEntrantes.add(cEntrante);

	}

	abstract protected T creaCarreteraEntrante(Carretera carretera);

	public void addCarreteraSalienteAlCruce(CruceGenerico<?> destino, Carretera carretera) {
		// añade una carretera saliente
		mapaCarreterasSalientes.put(destino, carretera);
	}

	public void entraVehiculoAlCruce(String idCarretera, Vehiculo vehiculo) {
		// añade el “vehiculo” a la carretera entrante “idCarretera”
		mapaCarreterasEntrantes.get(idCarretera).colaVehiculos.add(vehiculo);
	}

	@Override
	public void avanza(){
		if(carreterasEntrantes.size() == 0){
			return;
		} else if(indiceSemaforoVerde == -1) {
			actualizaSemaforos();
		} else{
			for (CarreteraEntrante carreteraEntrante : carreterasEntrantes) {
				if (carreteraEntrante.semaforo == true) {
					carreteraEntrante.avanzaPrimerVehiculo();
					actualizaSemaforos();
					return;
				}
			}
		}
	}

	abstract protected void actualizaSemaforos();
	
	@Override
	public String getNombreSeccion() {
		return "junction_report";
	}
	
	@Override
	protected void completaDetallesSeccion(IniSection is) {
		String queues = "";
		
		for (CarreteraEntrante c : carreterasEntrantes) {
			queues += "(" + c.carretera.getId() + "," +
					(c.semaforo == true ? "green" : "red") + ",[";
			for (Vehiculo v : c.colaVehiculos) {
				queues += v.getId();
				if (!v.equals(c.colaVehiculos.get(c.colaVehiculos.size() - 1))) {
					queues += ",";
				}
			}
						
			queues += "])" + (!c.equals(carreterasEntrantes.get(carreterasEntrantes.size() - 1)) ? "," : "");

		}
		is.setValue("queues", queues);
	}
}


