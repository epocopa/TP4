package es.ucm.fdi.model.events;

import es.ucm.fdi.model.ObjetosSimulacion.Coche;
import es.ucm.fdi.model.ObjetosSimulacion.CruceGenerico;
import es.ucm.fdi.model.ObjetosSimulacion.Vehiculo;

import java.util.List;

public class EventoNuevoCoche extends EventoNuevoVehiculo {
	protected int resistenciaKm;
	protected int duracionMaximaAveria;
	protected double probabilidadDeAveria;
	protected long numAleatorio;

	public EventoNuevoCoche(int tiempo, String id, int velocidadMaxima, String[] itinerario, int resistenciaKm, double probabilidadDeAveria, int duracionMaximaAveria, long numAleatorio) {
		super(tiempo, id, velocidadMaxima, itinerario);
		this.resistenciaKm = resistenciaKm;
		this.duracionMaximaAveria = duracionMaximaAveria;
		this.probabilidadDeAveria = probabilidadDeAveria;
		this.numAleatorio = numAleatorio;
	}

	@Override
	protected Vehiculo creaVehiculo(String id, int velocidadMaxima, List<CruceGenerico<?>> itinerario) {
		return new Coche(id, velocidadMaxima, resistenciaKm, probabilidadDeAveria, numAleatorio, duracionMaximaAveria , itinerario);
	}
}
