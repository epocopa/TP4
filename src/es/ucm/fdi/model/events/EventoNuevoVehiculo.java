package es.ucm.fdi.model.events;

import es.ucm.fdi.control.ErrorDeSimulacion;
import es.ucm.fdi.model.MapaCarreteras;
import es.ucm.fdi.model.ObjetosSimulacion.CruceGenerico;
import es.ucm.fdi.model.ObjetosSimulacion.Vehiculo;

import java.util.List;

import static es.ucm.fdi.util.ParserCarreteras.parseaListaCruces;

public class EventoNuevoVehiculo extends Evento {
	protected String id;
	protected Integer velocidadMaxima;
	protected String[] itinerario;

	public EventoNuevoVehiculo(int tiempo, String id, Integer velocidadMaxima, String[] itinerario) {
		super(tiempo);
		this.id = id;
		this.velocidadMaxima = velocidadMaxima;
		this.itinerario = itinerario;
	}

	@Override
	public void ejecuta(MapaCarreteras mapa) {
		List<CruceGenerico<?>> iti = parseaListaCruces (itinerario , mapa);
		if (iti == null || iti.size() < 2){
			throw  new ErrorDeSimulacion("Itinerario nulo o con menos de 2 cruces");
		} else {
			mapa.addVehiculo(id, creaVehiculo(id, velocidadMaxima, iti));
		}
	}

	protected Vehiculo creaVehiculo(String id, int velocidadMaxima, List<CruceGenerico<?>> itinerario){
		return new Vehiculo(id, velocidadMaxima, itinerario);
	}
}
