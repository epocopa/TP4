package es.ucm.fdi.model.events;

import es.ucm.fdi.model.MapaCarreteras;
import es.ucm.fdi.model.ObjetosSimulacion.Vehiculo;
import es.ucm.fdi.util.ParserCarreteras;

import java.util.List;

public class EventoAveriaCoche extends Evento{
	private int duracion;
	private String[] vehiculos;

	public EventoAveriaCoche(int duracion, String[] vehiculos, int tiempo) {
		super(tiempo);
		this.vehiculos = vehiculos;
		this.duracion = duracion;
	}

	@Override
	public void ejecuta(MapaCarreteras mapa) {
		List<Vehiculo> averiados = ParserCarreteras.parseaListaVehiculos(vehiculos, mapa);
		for (Vehiculo averiado : averiados) {
			averiado.setTiempoAveria(duracion);
		}
	}
}
