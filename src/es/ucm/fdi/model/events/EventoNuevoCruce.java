package es.ucm.fdi.model.events;

import es.ucm.fdi.model.MapaCarreteras;
import es.ucm.fdi.model.ObjetosSimulacion.Cruce;
import es.ucm.fdi.model.ObjetosSimulacion.CruceGenerico;

public class EventoNuevoCruce extends Evento {
	protected String id;

	public EventoNuevoCruce(int tiempo, String id) {
		super(tiempo);
		this.id = id;
	}

	@Override
	public void ejecuta(MapaCarreteras mapa) {
		mapa.addCruce(id, creaCruce());
	}

	protected CruceGenerico<?> creaCruce() {
		return new Cruce(id);
	}
}
