package es.ucm.fdi.model.events;

import es.ucm.fdi.model.ObjetosSimulacion.CruceCircular;
import es.ucm.fdi.model.ObjetosSimulacion.CruceGenerico;

public class EventoNuevoCruceCircular extends EventoNuevoCruce {
	protected Integer maxValorIntervalo;
	protected Integer minValorIntervalo;

	public EventoNuevoCruceCircular(int time, String id, int maxValorIntervalo, int minValorIntervalo) {
		super(time, id);
		this.maxValorIntervalo = maxValorIntervalo;
		this.minValorIntervalo = minValorIntervalo;
	}

	@Override
	protected CruceGenerico<?> creaCruce() {
		return new CruceCircular(id, minValorIntervalo, maxValorIntervalo);
	}
}
