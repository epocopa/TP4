package es.ucm.fdi.model.events;

import es.ucm.fdi.model.ObjetosSimulacion.Camino;
import es.ucm.fdi.model.ObjetosSimulacion.Carretera;
import es.ucm.fdi.model.ObjetosSimulacion.CruceGenerico;

public class EventoNuevoCamino extends EventoNuevaCarretera {

	public EventoNuevoCamino(int tiempo, String id, Integer velocidadMaxima, Integer longitud, String cruceOrigenId, String cruceDestinoId) {
		super(tiempo, id, velocidadMaxima, longitud, cruceOrigenId, cruceDestinoId);
	}


	@Override
	protected Carretera creaCarretera(CruceGenerico<?> cruceOrigen, CruceGenerico<?> cruceDestino) {
		return new Camino(id, longitud, velocidadMaxima, cruceOrigen, cruceDestino);
	}
}
