package es.ucm.fdi.model.events;

import es.ucm.fdi.model.MapaCarreteras;

public abstract class Evento {
	protected Integer tiempo;

	public Evento(int tiempo){
		this.tiempo = tiempo;
	}

	public Integer getTiempo() {
		return tiempo;
	}

	public  abstract void ejecuta(MapaCarreteras mapa);
}
