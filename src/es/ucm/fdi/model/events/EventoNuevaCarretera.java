package es.ucm.fdi.model.events;

import es.ucm.fdi.model.MapaCarreteras;
import es.ucm.fdi.model.ObjetosSimulacion.Carretera;
import es.ucm.fdi.model.ObjetosSimulacion.CruceGenerico;

public class EventoNuevaCarretera extends Evento {
	protected String id;
	protected Integer velocidadMaxima;
	protected Integer longitud;
	protected String cruceOrigenId;
	protected String cruceDestinoId;

	public EventoNuevaCarretera(int tiempo, String id, Integer velocidadMaxima, Integer longitud, String cruceOrigenId, String cruceDestinoId) {
		super(tiempo);
		this.id = id;
		this.velocidadMaxima = velocidadMaxima;
		this.longitud = longitud;
		this.cruceOrigenId = cruceOrigenId;
		this.cruceDestinoId = cruceDestinoId;
	}

	@Override
	public void ejecuta(MapaCarreteras mapa) {
		CruceGenerico<?> origen = mapa.getCruce(cruceOrigenId);
		CruceGenerico<?> destino = mapa.getCruce(cruceDestinoId);
		Carretera c = creaCarretera(origen, destino);
		mapa.addCarretera(id, origen, c, destino);
	}

	protected Carretera creaCarretera(CruceGenerico<?> cruceOrigen, CruceGenerico<?> cruceDestino) {
		return new Carretera(id, longitud, velocidadMaxima, cruceOrigen, cruceDestino);
	}
}
