package es.ucm.fdi.model.events;

import es.ucm.fdi.model.ObjetosSimulacion.Autopista;
import es.ucm.fdi.model.ObjetosSimulacion.Carretera;
import es.ucm.fdi.model.ObjetosSimulacion.CruceGenerico;

public class EventoNuevaAutopista extends EventoNuevaCarretera {
	protected Integer numCarriles;

	public EventoNuevaAutopista(int tiempo, String id, Integer velocidadMaxima, Integer longitud, String cruceOrigenId, String cruceDestinoId, Integer numCarriles) {
		super(tiempo, id, velocidadMaxima, longitud, cruceOrigenId, cruceDestinoId);
		this.numCarriles = numCarriles;
	}


	@Override
	protected Carretera creaCarretera(CruceGenerico<?> cruceOrigen, CruceGenerico<?> cruceDestino) {
		return new Autopista(id, longitud, velocidadMaxima, cruceOrigen, cruceDestino, numCarriles);
	}
}
