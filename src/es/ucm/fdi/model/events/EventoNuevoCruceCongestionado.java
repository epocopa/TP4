package es.ucm.fdi.model.events;

import es.ucm.fdi.model.ObjetosSimulacion.CruceCongestionado;
import es.ucm.fdi.model.ObjetosSimulacion.CruceGenerico;

public class EventoNuevoCruceCongestionado extends EventoNuevoCruce {
	public EventoNuevoCruceCongestionado(int tiempo, String id) {
		super(tiempo, id);
	}

	@Override
	protected CruceGenerico<?> creaCruce() {
		return new CruceCongestionado(id);
	}
}
