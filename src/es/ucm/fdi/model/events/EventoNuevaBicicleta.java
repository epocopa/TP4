package es.ucm.fdi.model.events;

import es.ucm.fdi.model.ObjetosSimulacion.Bicicleta;
import es.ucm.fdi.model.ObjetosSimulacion.CruceGenerico;
import es.ucm.fdi.model.ObjetosSimulacion.Vehiculo;

import java.util.List;

public class EventoNuevaBicicleta extends EventoNuevoVehiculo {

	public EventoNuevaBicicleta(int tiempo, String id, int velocidadMaxima, String[] itinerario) {
		super(tiempo, id, velocidadMaxima, itinerario);
	}

	@Override
	protected Vehiculo creaVehiculo(String id, int velocidadMaxima, List<CruceGenerico<?>> itinerario) {
		return new Bicicleta(id, velocidadMaxima, itinerario);
	}
}
