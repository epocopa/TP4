package es.ucm.fdi.model;

import es.ucm.fdi.control.ErrorDeSimulacion;
import es.ucm.fdi.model.ObjetosSimulacion.Carretera;
import es.ucm.fdi.model.ObjetosSimulacion.CruceGenerico;
import es.ucm.fdi.model.ObjetosSimulacion.Vehiculo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapaCarreteras {
	private List<Carretera> carreteras;
	private List<CruceGenerico<?>> cruces;
	private List<Vehiculo> vehiculos;

	// estructuras para agilizar la búsqueda (id,valor)
	private Map<String, Carretera> mapaDeCarreteras;
	private Map<String, CruceGenerico<?>> mapaDeCruces;
	private Map<String, Vehiculo> mapaDeVehiculos;

	public MapaCarreteras() {
		carreteras = new ArrayList<>();
		cruces = new ArrayList<>();
		vehiculos = new ArrayList<>();
		mapaDeCarreteras = new HashMap<>();
		mapaDeCruces = new HashMap<>();
		mapaDeVehiculos = new HashMap<>();
	}

	public void addCruce(String idCruce, CruceGenerico<?>  cruce) {
		if (mapaDeCruces.containsKey(idCruce)) {
			throw new ErrorDeSimulacion("El cruce ya existe en el mapa");
		} else {
			cruces.add(cruce);
			mapaDeCruces.put(idCruce, cruce);
		}
	}

	public void addVehiculo(String idVehiculo, Vehiculo vehiculo) {
		if (mapaDeVehiculos.containsKey(idVehiculo)) {
			throw new ErrorDeSimulacion("El vehiculo ya existe en el mapa");
		} else {
			vehiculos.add(vehiculo);
			mapaDeVehiculos.put(idVehiculo, vehiculo);
			vehiculo.moverASiguienteCarretera();
		}
	}

	public void addCarretera(String idCarretera, CruceGenerico<?> origen, Carretera carretera, CruceGenerico<?> destino) {
		if (mapaDeCarreteras.containsKey(idCarretera)) {
			throw new ErrorDeSimulacion("La carretera ya existe en el mapa");
		} else {
			carreteras.add(carretera);
			mapaDeCarreteras.put(idCarretera, carretera);
			origen.addCarreteraSalienteAlCruce(destino, carretera);
			destino.addCarreteraEntranteAlCruce(idCarretera, carretera);			
		}
	}

	public String generateReport(int time) {
		String report = "";
		for (CruceGenerico<?> c : cruces){
			report += c.generarInforme(time) + "\n";
		}
		for (Carretera c : carreteras){
			report += c.generarInforme(time) + "\n";

		}
		for (Vehiculo v : vehiculos){
			report += v.generarInforme(time) + "\n";
		}

		return report;
	}

	public void actualizar() {

		for (Carretera c : carreteras){
			c.avanza();
		}
		for (CruceGenerico<?>  c : cruces){
			c.avanza();
		}
	}

	public CruceGenerico<?>  getCruce(String id) {
		CruceGenerico<?>  c = mapaDeCruces.get(id);
		if (c != null){
			return c;
		} else {
			throw new ErrorDeSimulacion("El cruce no existe");
		}
	}

	public Vehiculo getVehiculo(String id) {
		Vehiculo v = mapaDeVehiculos.get(id);
		if (v != null){
			return v;
		} else {
			throw new ErrorDeSimulacion("El vehiculo no existe");
		}
	}

	public Carretera getCarretera(String id) {
		Carretera v = mapaDeCarreteras.get(id);
		if (v != null){
			return v;
		} else {
			throw new ErrorDeSimulacion("La carretera no existe");
		}
	}
}
