package es.ucm.fdi.control;

import es.ucm.fdi.ini.Ini;
import es.ucm.fdi.ini.IniSection;
import es.ucm.fdi.model.SimuladorTrafico;
import es.ucm.fdi.model.events.Evento;
import es.ucm.fdi.util.ParserEventos;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class Controlador {
	private SimuladorTrafico simulador;
	private InputStream ficheroEntrada;
	private OutputStream ficheroSalida;
	private int pasosSimulacion;
	List<IniSection> iniSections;


	public Controlador(SimuladorTrafico sim, Integer limiteTiempo, InputStream is, OutputStream os) {
		simulador = sim;
		this.pasosSimulacion = limiteTiempo;
		ficheroEntrada = is;
		ficheroSalida = os;
	}

	public void ejecuta() {
		cargarEventos(ficheroEntrada);
		simulador.ejecuta(pasosSimulacion, ficheroSalida);
	}

	private void cargarEventos(InputStream inStream) {
		Ini ini;
		try {
			ini = new Ini(inStream);
		} catch (IOException e) {
			throw new ErrorDeSimulacion("Error en la lectura de eventos: " + e);
		}

		for (IniSection sec : ini.getSections()){
			Evento e = ParserEventos.parseaEvento(sec);
			if (e != null) {
				simulador.insertaEvento(e);
			} else {
				throw new ErrorDeSimulacion("Evento desconocido: " + sec.getTag());
			}
		}
	}
}
