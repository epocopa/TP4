package es.ucm.fdi.util;

import es.ucm.fdi.model.MapaCarreteras;
import es.ucm.fdi.model.ObjetosSimulacion.CruceGenerico;
import es.ucm.fdi.model.ObjetosSimulacion.Vehiculo;

import java.util.ArrayList;
import java.util.List;

public class ParserCarreteras {
	public static List<CruceGenerico<?>> parseaListaCruces(String[] itinerario, MapaCarreteras mapa){
		List<CruceGenerico<?>> lista = new ArrayList<>();
		for (String cruce : itinerario) {
			lista.add(mapa.getCruce(cruce));
		}
		return lista;
	}

	public static List<Vehiculo> parseaListaVehiculos(String[] vehiculos, MapaCarreteras mapa){
		ArrayList<Vehiculo> lista = new ArrayList<>();
		for (String vehiculo : vehiculos) {
			lista.add(mapa.getVehiculo(vehiculo));
		}
		return lista;
	}
}
