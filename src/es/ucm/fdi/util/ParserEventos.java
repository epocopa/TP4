package es.ucm.fdi.util;

import es.ucm.fdi.constructor.*;
import es.ucm.fdi.ini.IniSection;
import es.ucm.fdi.model.events.Evento;

public class ParserEventos {
	private static ConstructorEventos[] eventos = {
			new ConstructorEventoNuevoCruce(),
			new ConstructorEventoNuevaCarretera(),
			new ConstructorEventoNuevoVehiculo(),
			new ConstructorEventoAveriaCoche(),
			new ConstructorEventoNuevaAutopista(),
			new ConstructorEventoNuevoCamino(),
			new ConstructorEventoNuevoCoche(),
			new ConstructorEventoNuevaBicicleta(),
			new ConstructorEventoNuevoCruceCircular(),
			new ConstructorEventoNuevoCruceCongestionado()
	};

	public static Evento parseaEvento(IniSection sec){
		int i = 0;
		boolean seguir = true;
		Evento e = null;
		while (i < eventos.length && seguir){
			e = eventos[i].parser(sec);
			if (e != null) {
				seguir = false;
			} else {
				i++;
			}
		}
		return e;
	}
}
